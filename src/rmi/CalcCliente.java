package rmi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class CalcCliente {
	public static void main(String[] args) throws RemoteException, NotBoundException {
		Registry reg = LocateRegistry.getRegistry("localhost",9900);
		ICale calc = (ICale) reg.lookup("calc1");
		
		System.out.println(calc.add(5,3));
	}
}
