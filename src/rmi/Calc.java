package rmi;

import java.rmi.RemoteException;

public class Calc implements ICale {

	@Override
	public int add(int x, int y) throws RemoteException {
		
		return x+y;
	}

	@Override
	public int mult(int x, int y) throws RemoteException {
		return x*y;
	}

}
