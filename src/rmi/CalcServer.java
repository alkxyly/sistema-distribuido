package rmi;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class CalcServer {
	public static void main(String[] args) throws RemoteException {
		Calc calc =  new Calc();
		ICale stub =  (ICale) UnicastRemoteObject.exportObject(calc,7988);
		Registry reg = LocateRegistry.createRegistry(9900); // registrar uma referencia remota da calculadora usa o Registry
		reg.rebind("calc1",stub);
	}
}
